// ------------------------------------------------------------
// Helpful? tips???
// ------------------------------------------------------------
extend class hdplayerpawn{
	double specialtipalpha;
	string specialtip;
	static void gametip(actor caller,string message){
		let hdp=hdplayerpawn(caller);
		if(hdp)hdp.usegametip(message);
		else caller.A_Log(message,true);
	}
	void usegametip(string arbitrarystring){
		arbitrarystring.replace("\r","");
		arbitrarystring.replace("\n\n\n","\n");
		arbitrarystring.replace("\n\n","\n");
		specialtipalpha=1001.;
		specialtip=arbitrarystring;
	}
	static void massgametip(string arbitrarystring){
		for(int i=0;i<MAXPLAYERS;i++){
			let hdp=hdplayerpawn(players[i].mo);
			if(hdp)hdp.usegametip(arbitrarystring);
		}
	}
	void showgametip(){
		if(
			!player
			||!hd_helptext
			||!hd_helptext.getbool()
		)return;
		specialtip=StringTable.Localize("$TIP_TIP")..StringTable.Localize("$TIP_"..random[tiprand](0,31));
		specialtipalpha=1001.;
		A_Log(specialtip,true);
	}
}
