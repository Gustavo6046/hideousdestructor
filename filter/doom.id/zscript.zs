//Stuff specific to id
version "4.11"


#include "zscript/mob_lostsoul.zs"
#include "zscript/keen.zs"

//remove pistol zombie
class ZombieHideousIdTrooper:RandomSpawner replaces ZombieHideousTrooper{
	default{
		dropitem "ZombieAutoStormtrooper",256,100;
		dropitem "ZombieSemiStormtrooper",256,20;
		dropitem "ZombieSMGStormtrooper",256,10;
		dropitem "EnemyHERP",256,1;
	}
}


