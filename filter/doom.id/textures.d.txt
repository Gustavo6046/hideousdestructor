
//The original HD ammobox concept: id ammo box turned upside down
sprite AMMOA0,28,16{
	offset 12,13
	patch AMMOA0,0,0{}
}
sprite OWWVA0,28,16{
	offset 12,13
	patch AMMOA0,0,0{}
	patch AMMOA0,0,3{rotate 180}
}

sprite CMAGA0,9,11{
	offset 5,11
	patch CLIPA0,0,0{}
}

sprite CELLA0,17,12{
	offset 8,12
	patch CELLA0,0,0{}
}

//Shells
sprite SHELA0,15,7{
	offset 7,7
	patch SHELA0,0,0{flipy;translation "161:167=85:100"}
}
sprite SHL1A0,3,7{
	offset 3,7
	patch SHELA0,0,0{translation "161:167=85:100"}
}

sprite PUF2A0,5,5{
	xscale 0.12
	yscale 0.12
	offset 3,5
	patch PUFFA0,0,0{
		translation "0:255=%[0,0,0]:[0.6,0.6,0.6]"
		style "reversesubtract"
	}
}
sprite PUF2B0,9,8{
	xscale 0.16
	yscale 0.16
	offset 5,7
	patch PUFFB0,0,0{
		translation "0:255=%[0,0,0]:[0.6,0.6,0.6]"
		style "reversesubtract"
	}
}
sprite PUF2C0,12,11{
	xscale 0.2
	yscale 0.2
	offset 6,10
	patch PUFFC0,0,0{
		translation "80:111=96:111"
		style "reversesubtract"
	}
}
sprite PUF2D0,15,15{
	xscale 0.24
	yscale 0.24
	offset 7,13
	patch PUFFD0,0,0{
		translation "80:111=96:111"
		style "reversesubtract"
	}
}

//Yokai - make it look a little better in id
sprite YOKAA0,25,25{
	offset 13,25
	patch PINSA0,0,0{translation "176:191=29:47","192:207=160:167","240:247=188:191"}
}
sprite YOKAB0,25,25{
	offset 13,25
	patch PINSB0,0,0{translation "176:191=29:47","192:207=160:167","240:247=188:191"}
}
sprite YOKAC0,25,25{
	offset 13,25
	patch PINSC0,0,0{translation "176:191=29:47","192:207=160:167","240:247=188:191"}
}
sprite YOKAD0,25,25{
	offset 13,25
	patch PINSD0,0,0{translation "176:191=29:47","192:207=160:167","240:247=188:191"}
}


//Flaming barrel - id sprite is off center
sprite optional FCANA0,37,53{
	offset 12,51
	patch FCANA0,0,0{}
}
sprite optional FCANB0,34,53{
	offset 12,51
	patch FCANB0,0,0{}
}
sprite optional FCANC0,36,51{
	offset 12,49
	patch FCANC0,0,0{}
}


//Backpack
sprite BPAKB0,29,22{
	offset 14,22
	xscale 0.83
	yscale 1.2
	patch BPAKA0,0,0{rotate 270}
}
sprite BPAKC0,22,30{
	offset 11,30
	patch STASQGR,4,0{}
	patch BPAKA0,0,1{}
}


//Chainsaw
sprite BEVGA0,140,55{
	offset -122,-166
	xscale 1.3
	yscale 1.3
	patch SAWGA0,0,0 {}
}
sprite BEVGB0,140,55{
	offset -122,-166
	xscale 1.3
	yscale 1.3
	patch SAWGB0,0,0 {}
}
sprite BEVGC0,153,89{
	offset -122,-136
	xscale 1.3
	yscale 1.3
	patch SAWGC0,0,0 {}
}
sprite BEVGD0,154,89{
	offset -122,-136
	xscale 1.3
	yscale 1.3
	patch SAWGD0,0,0 {}
}


//Pistol -id
sprite PI1GA0,57,62{
	offset -126,-106
	patch PISGA0,0,0{}
}
sprite PI1GB0,79,82{
	offset -104,-110
	patch PISGB0,0,0{}
}
sprite PI1GC0,66,81{
	offset -119,-109
	patch PISGC0,0,0{}
}
sprite PI1GD0,61,81{
	offset -125,-108
	patch PISGD0,0,0{}
}
sprite PI1FA0,41,38{
	offset -140,-90
	patch PISFA0,0,0{}
}
//right
sprite PI2GA0,57,62{
	offset -137,-106
	patch PISGA0,0,0{flipx}
}
sprite PI2GB0,79,82{
	offset -136,-110
	patch PISGB0,0,0{flipx}
}
sprite PI2GC0,66,81{
	offset -134,-109
	patch PISGC0,0,0{flipx}
}
sprite PI2GD0,61,81{
	offset -133,-108
	patch PISGD0,0,0{flipx}
}
sprite PI2FA0,41,38{
	offset -138,-90
	patch PISFA0,0,0{flipx}
}


//SMG
sprite SMGNB0,52,14{
	offset 26,14
	patch SMSNB0,0,-2{}
}
sprite SMSNA0,52,20{
	offset 26,20
	patch SMGNM0,28,8{}
	patch SMSNB0,0,0{}
}
sprite SMGNA0,52,18{
	offset 26,18
	patch SMGNM0,28,6{}
	patch SMGNB0,0,0{}
}
sprite SMGGA0,118,105{
	offset -138,-135
	xscale 1.3
	yscale 1.3
	patch SMGGA0,0,0{}
}
sprite SMGGB0,118,105{
	offset -131,-140
	xscale 1.26
	yscale 1.3
	patch SMGGA0,0,0{}
}
sprite SMSGA0,118,105{
	offset -138,-135
	xscale 1.3
	yscale 1.3
	patch SMGGA0,0,0{}
	patch SMGS,51,24{}
}
sprite SMSGB0,118,105{
	offset -131,-140
	xscale 1.26
	yscale 1.3
	patch SMSGA0,0,0{}
}
sprite SMGFA0,80,46{
	offset -232,-105
	xscale 1.7
	yscale 1.3
	patch SMGFA0,0,0{}
	patch SMGFA0,0,0{flipx}
}


//Shotgun - id
sprite SHTGG0,119,121{
	offset -43,-47
	patch SHTGB0,0,0{}
}
sprite SHTGH0,87,151{
	offset -30,-17
	patch SHTGC0,0,0{}
}
sprite SHTGI0,113,131{
	offset -29,-37
	patch SHTGD0,0,0{}
}
sprite HUNTA0,65,12{
	offset 24,11
	patch SGUNPK,0,0{}
}
sprite HUNTB0,65,12{
	offset 24,11
	patch SGUNPK,0,-12{}
}
sprite HUNTC0,65,12{
	offset 24,11
	patch SGUNPK,0,-24{}
}
sprite HUNTD0,65,12{
	offset 24,11
	patch SGUNPK,0,-36{}
}
sprite HUNTE0,65,12{
	offset 24,11
	patch SGUNPK,0,-48{}
}
sprite HUNTF0,65,12{
	offset 24,11
	patch SGUNPK,0,-60{}
}
sprite HUNTG0,65,12{
	offset 24,11
	patch SGUNPK,0,-72{}
}
sprite SLAYA0,58,12{
	offset 25,11
	patch SGUNPK,-66,0{}
}
sprite SLAYB0,58,12{
	offset 25,11
	patch SGUNPK,-66,-12{}
}
sprite SLAYC0,58,12{
	offset 25,11
	patch SGUNPK,-66,-24{}
}
sprite SLAYD0,58,12{
	offset 25,11
	patch SGUNPK,-66,-36{}
}
sprite SLAYE0,58,12{
	offset 25,11
	patch SGUNPK,-66,-48{}
}
sprite SLAYF0,58,12{
	offset 25,11
	patch SGUNPK,-66,-60{}
}
sprite SLAYG0,58,12{
	offset 25,11
	patch SGUNPK,-66,-72{}
}

//SSG
sprite optional SH2GK0,59,55{
	offset -116,-105
	xscale 0.90
	yscale 0.92
	patch SH2GA0,0,0{}
}
sprite optional SH2GB0,81,80{
	offset -130,-118
	xscale 0.95
	yscale 0.9
	patch SHT2G0,0,0{}
}
sprite optional SH2GC0,77,43{
	offset -150,-124
	patch SH2GE0,-124,0{}
}
sprite optional SH2GD0,240,63{
	offset -45,-125
	patch SHT2E0,0,0{}
}
sprite optional SH2GE0,88,51{
	offset -120,-117
	patch SHT2F0,0,0{}
}
sprite optional SH2GF0,77,85{
	offset -120,-100
	patch SHT2G0,0,0{flipx}
}


//Vulcanette
sprite GTLGA0,114,81{
	offset -182,-176
	xscale 1.5
	yscale 1.5
	patch CHGGA0,0,0{}
}
sprite GTLGB0,114,81{
	offset -182,-178
	xscale 1.5
	yscale 1.5
	patch CHGGB0,0,0 {}
}
sprite VULFA0,86,46{
	offset -196,-157
	xscale 1.5
	yscale 1.5
	patch CHGFA0,0,0{}
}
sprite VULFB0,85,47{
	offset -198,-156
	xscale 1.5
	yscale 1.5
	patch CHGFB0,0,0{}
}



//H.E.R.P.
sprite HERGA0,114,81{
	offset -150,-196
	xscale 1.3
	yscale 1.3
	patch CHGGA0,0,0{translation "80:111=100:111";}
}
sprite HERGB0,114,81{
	offset -150,-198
	xscale 1.3
	yscale 1.3
	patch CHGGB0,0,0{translation "80:111=100:111";}
}
sprite HERFA0,86,46{
	offset -162,-177
	xscale 1.3
	yscale 1.3
	patch CHGFA0,0,0{}
}
sprite HERFB0,85,47{
	offset -164,-176
	xscale 1.3
	yscale 1.3
	patch CHGFB0,0,0{}
}

sprite B9KGA0,170,84{
	offset -75,-95
	patch BFGGA0,0,0{}
}
sprite B9KGB0,170,84{
	offset -75,-95
	patch BFGGB0,0,0{}
}
sprite B9KGC0,156,83{
	offset -83,-95
	patch BFGGC0,0,0{}
}
sprite B9KFA0,82,40{
	offset -119,-77
	patch BFGFA0,0,0{}
}
sprite B9KFB0,139,67{
	offset -91,-56
	patch BFGFB0,0,0{}
}


//RL
sprite LAUGA0,87,79{
	offset -117,-120
	patch MISGA0,0,0{}
}
sprite LAUGB0,102,75{
	offset -109,-123
	patch MISGB0,0,0{}
}
sprite LAUFA0,62,34{
	offset -134,-104
	patch MISFA0,0,0{}
}
sprite LAUFB0,73,51{
	offset -123,-100
	patch MISFB0,0,0{}
}
sprite LAUFC0,88,58{
	offset -114,-92
	patch MISFC0,0,0{}
}
sprite LAUFD0,105,79{
	offset -108,-80
	patch MISFD0,0,0{}
}


//Brontornis
sprite BLSGA0,80,62{
	offset -125,-106
	patch BLSGA0,0,0{flipx}
}
sprite BLSGB0,89,73{
	offset -120,-96
	patch BLSGB0,0,0{flipx}
}
sprite BLSFA0,90,97{
	offset -115,-63
	patch BLSFA0,0,0{flipx}
}
sprite BLSGC0,93,50{
	offset -120,-118
	patch BLSGC0,0,0{flipx}
}
sprite BLSGD0,98,32{
	offset -115,-135
	patch BLSGD0,0,0{flipx}
}
sprite BLSGE0,79,48{
	offset -140,-120
	patch BLSGE0,0,0{flipx}
}


//Medikit
sprite MEDIA0,28,19{
	offset 14,19
	patch MEDIA0,0,0{translation "176:191=120:127"}
}
sprite STIMA0,14,15{
	offset 7,15
	patch STIMA0,0,0{translation "176:191=124:127"}
}


//Potion bottle
sprite ECTPA0,14,18{
	offset 7,18
	patch BON1A0,0,0{}
}
sprite ECTPB0,14,18{
	offset 7,18
	patch BON1B0,0,0{}
}
sprite ECTPC0,14,18{
	offset 7,18
	patch BON1C0,0,0{}
}
sprite ECTPD0,14,18{
	offset 7,18
	patch BON1D0,0,0{}
}
sprite ECTPE0,14,18{
	offset 7,18
	patch BON1A0,0,0{}
}
sprite ECTPF0,14,18{
	offset 7,9
	patch BON1D0,0,0{translation "ice"}
}


//Squad summoner
sprite PRIFA0,55,22{
	offset 24,20
	patch ARM1A0,6,0 {translation "112:117=144:151","118:124=108:111"}
	patch BRLLD0,0,7 {}
	patch BON2A0,4,7 {}
}


//Radsuit
graphic SUITC0,15,15{
	patch SUITA0,-5,-32{}
}


//Armour
sprite ARMSA0,31,17{
	offset 15,17
	patch ARM1A0,0,0{translation "112:120=152:159","121:127=9:12"}
}
sprite ARMSB0,31,17{
	offset 0,-8
	patch ARMSA0,0,0{}
}
sprite ARMCB0,31,17{
	offset 0,-8
	patch ARMCA0,0,0{}
}
graphic ARMER0,31,17{
	offset 15,17
	patch ARMSA0,0,0{translation "0:255=%[0,0,0]:[0.3,0.14,0]"}
}
graphic ARMER1,31,17{
	offset 15,17
	patch ARMCA0,0,0{translation "0:255=%[0,0,0]:[0.3,0.14,0]"}
}
