// ------------------------------------------------------------
// Lost Soul
// ------------------------------------------------------------
class FlyingSkull:HDMobBase replaces Hatchling{
	default{
		mass 50;
		painchance 256;
		monster;
		+float +nogravity +missilemore +dontfall
		+noicedeath +noextremedeath
		attacksound "skull/melee";
		painsound "skull/pain";
		deathsound "skull/death";
		tag "$cc_lost";

		+noblood +pushable -floorclip +avoidmelee
		+noforwardfall
		+bright
		+hdmobbase.noshootablecorpse
		+hdmobbase.chasealert
		+hdmobbase.novitalshots
		obituary "$OB_SKULL";
		activesound "";
		scale 0.6;
		radius 9;
		height 22;
		speed 4;
		damage 3;
		health 70;
		alpha 1.0;
		renderstyle "normal";
		maxtargetrange 245;
	}
	override void postbeginplay(){
		super.postbeginplay();
		A_GiveInventory("ImmunityToFire");
		resize(0.8,1.3);
		latched=null;
		lastpointinmap=pos;
	}
	override void tick(){
		if(isfrozen())return;

		if(
			!level.ispointinlevel(pos)
			||!checkmove(pos.xy,PCM_DROPOFF|PCM_NOACTORS)
		){
			setorigin(lastpointinmap,true);
			setz(clamp(pos.z,floorz,ceilingz-height));
		}else lastpointinmap=pos;

		//skip teleport checks the stupid way since we're not replacing the tick entirely like the babuin
		bnotrigger=!!latched;

		super.tick();
		A_TakeInventory("Heat");
		if(!bcorpse&&!(level.time&(1|2|4))){
			let fff=spawn("HDFlameRed",pos+((cos(angle),sin(angle))*-6,frandom(2,8)),ALLOW_REPLACE);
			fff.vel+=vel*0.8;
		}
	}
	void A_SkullChase(){
		A_HDChase();
		vel.z+=frandom(-0.3,0.3);
	}
	void A_SkullStrafe(){
		A_FaceTarget(16,16);
		spawn("HDSmoke",pos,ALLOW_REPLACE);
		A_ChangeVelocity(cos(pitch),randompick(-1,1)*frandom(3,6),0,CVF_RELATIVE);
	}
	void A_SkullLaunch(){
		latched=null;
		A_StartSound("skull/melee",CHAN_VOICE);
		spawn("HDSmoke",pos,ALLOW_REPLACE);
		A_FaceTarget(16,16);
		A_ChangeVelocity(cos(pitch)*20,0,-sin(pitch)*20,CVF_RELATIVE);
		setstatelabel("flying");
	}
	void A_SkullFlying(){
		vel*=0.95;
		if(vel dot vel<(10*10)){
			setstatelabel("see");
			return;
		}
		if(!CheckMove(pos.xy+vel.xy,1)){
			bool bounce=false;
			if(blockingline){
				bounce=true;
			}else if(!!blockingmobj){
				actor bmj=blockingmobj;
				if(
					bmj!=target
					||absangle(angleto(bmj),angle)>30
				){
					bounce=true;
					Heat.Inflict(bmj,5);
					bmj.vel+=vel*min(mass/max(1,bmj.mass),0.9);
				}else{
					A_FaceTarget();
					latched=bmj;
					latchangle=deltaangle(bmj.angle,bmj.angleto(self));
					latchdist=frandom(0.95,1.)*(latched.radius+radius);
					latchz=pos.z-latched.height-latched.pos.z;
					latchedlastangle=latched.angle;
					Heat.Inflict(bmj,12);
					latched.damagemobj(self,self,random(2,7),"Teeth");
					setstatelabel("latched");
				}
			}

			if(bounce){
				if(vel.x||vel.y)vel.xy=rotatevector(vel.xy,frandom(120,240));
				vel.z*=frandom(0.4,0.9)*randompick(-1,1);
				A_StartSound("weapons/fragknock",CHAN_BODY);
				forcepain(self);
			}
		}
	}
	void A_SkullLatched(){
		if(!latched||latched.health<1){
			latched=null;
			setstatelabel("see");
			return;
		}
		setz(latched.height+latched.pos.z+latchz);
		if(
			absangle(latched.angle,latchedlastangle)>30
			||!trymove(latched.pos.xy+angletovector(latched.angle+latchangle,latchdist),true)
		){
			target=latched; //just in case it was forgotten somehow
			latched=null;
			if(vel.x||vel.y)vel.xy=rotatevector(vel.xy,frandom(120,240));
			vel.z*=frandom(0.4,0.9)*randompick(-1,1);
			A_StartSound("weapons/fragknock",CHAN_BODY);
			forcepain(self);
			return;
		}
		//omnomnom
		A_FaceTarget();
		latchedlastangle=latched.angle;
		setorigin(pos+(frandom(-1,1),frandom(-1,1),frandom(-1,1)),true);
		Heat.Inflict(latched,frandom(6,12));
		latched.angle+=frandom(-2,2);
		latched.pitch+=frandom(-2,2);
		if(!random(0,6))latched.damagemobj(self,self,random(0,2),"Teeth");
	}
	override bool cancollidewith(actor other,bool passive){
		return
			other!=latched
			&&(
				other!=master
				||getage()>40
			)
		;
	}
	actor latched;
	double latchdist;
	double latchz;
	double latchangle;
	double latchedlastangle;
	vector3 lastpointinmap;
	states{
	spawn:
		SKUL AB 6 {
			if(!random(0,63))A_Vocalize("skull/idlescream");
			else if(!random(0,31))A_Vocalize(activesound);
			if(random(0,3))A_HDLook();
			else A_HDWander();
		}
		loop;
	see:
		SKUL AB 3 A_SkullChase();
		loop;
	strafe:
		SKUL A 0 A_SkullStrafe();
		SKUL ABAB 2 bright;
		---- A 0 setstatelabel("see");
	missile:
		SKUL A 0 A_Jump(64,"strafe");
		SKUL A 0 A_Jump(200,2);
		SKUL A 0 A_SkullStrafe();
		SKUL ABABAB 2;
		SKUL B 2 A_FaceTarget(0,0);
		SKUL C 2 A_SkullLaunch();
	flying:
		SKUL CCDD 1 A_SkullFlying();
		loop;
	latched:
		SKUL CD 1 A_SkullLatched();
		loop;
	pain:
		SKUL E 3 bright;
		SKUL E 3 A_Pain();
		---- A 0 setstatelabel("see");
	death:
		SKUL E 2 bright{
			A_SetTranslucent(1,1);
			vel.z++;
		}
		TNT1 AAA 0 A_SpawnItemEx("HDSmokeChunk",0,0,3,
			vel.x+frandom(-4,4),vel.y+frandom(-4,4),vel.z+frandom(1,6),
			0,SXF_ABSOLUTE|SXF_ABSOLUTEMOMENTUM
		);
		SKUL G 2 A_NoBlocking();
		SKUL H 2 A_Scream();
		SKUL HHH 0 A_SpawnItemEx("HDSmoke",
			frandom(-2,2),frandom(-2,2),frandom(4,8),
			vel.x,vel.y,vel.z+random(1,2),
			0,SXF_ABSOLUTE|SXF_ABSOLUTEMOMENTUM
		);
		SKUL HHHHHHHHH 0 A_SpawnItemEx("BigWallChunk",
			frandom(-4,4),frandom(-4,4),frandom(2,14), 
			vel.x+frandom(-4,4),vel.y+frandom(-4,4),vel.z+frandom(-4,10),
			random(0,360),SXF_ABSOLUTEMOMENTUM|SXF_ABSOLUTE|SXF_SETTARGET
		);
		SKUL I 1{
			HDActor.HDBlast(self,
				blastradius:96,blastdamage:random(1,12),blastdamagetype:"hot",
				immolateradius:96,immolateamount:random(4,20),immolatechance:32
			);
		}
		SKUL JJJJ 1 A_FadeOut(0.2);
		stop;
	}
}
