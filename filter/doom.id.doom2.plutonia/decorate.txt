//different replacements for Plutonia
actor plutochaingunner:randomspawner replaces chaingunguy{
	dropitem "vulcanettezombie",256,2
	dropitem "undeadjackbootman",256,2
	dropitem "zombiestormtrooper",256,2
	dropitem "undeadrifleman",256,1
	dropitem "enemyherp",256,1
	dropitem "enemyderp",256,1
}
actor plutovile:randomspawner replaces archvile{
	dropitem "necromancer",256,2
	dropitem "baron",256,1
	dropitem "knave",256,1
	dropitem "hdgoon",256,1
	dropitem "healerimp",256,1
}
